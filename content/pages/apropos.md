---
title : "À propos"
---

# À propos de l'atelier

## Descriptif

Aujourd’hui de nombreux outils ou applications de rédaction, d’édition et de publication de contenus textuels sont accessibles au grand public sur le Web, mais sans nécessairement prendre en compte les besoins et les pratiques d’écriture des communautés de recherche. La question des formats se place au cœur de la structuration du texte numérique et il est important, pour qui veut produire des connaissances organisées, pérennes et transmissibles, de comprendre les enjeux culturels, techniques et politiques des formats.

Dans le cadre de 3 séances, plusieurs principes d’édition numériques seront présentés et implémentés pour constituer une culture critique sur les formats d’édition : balisage sémantique, bibliographie structurée, chaîne modulaire, etc.

## Objectif

L’objectif de cet atelier sur les formats d’écriture et les différentes chaînes éditoriales de production de contenu est la découverte et la prise en main d’outils pour la rédaction numérique en vue de la publication[^1].

[^1]: Aucune connaissance technique préalable n'est requise pour suivre le cours.

L’atelier n’a pas seulement pour but de former à des outils spécifiques des Humanités numériques mais vise à offrir aux étudiant·e·s les connaissances théoriques et pratiques fondamentales pour leur permettre d’explorer les potentialités du numérique, de constituer une réflexion critique sur les outils numériques et de développer leurs propres pratiques d’écriture et de publication.

## Calendrier 

L'atelier aura lieu les Mardis 5, 11 et 18 septembre de 9h30 à 11h29 et 12h30 à 15h29.

| Séance | Date  | Thème |
|:-:|---|---|
| 1  | 5 septembre | Publication scientifique |
| 2  | 12 septembre  | Travail collaboratif |
| 3 | 19 septembre | Publication Web |

### Programme détaillé 

### Séance 1 - Publication scientifique 

9h30-10h : Présentation & Introduction de l'atelier

10h-11h : Édition savante & Formats d’écriture

11h-11h29 : Langage de balisage & Initiation à Markdown

12h30-14h : Chaînes éditoriales & Initiation à Stylo

14h-15h : Références structurées & Initiation à Zotero

15h-15h29 : Présentation de l’exercice à faire pour la séance 2

#### Séance 2 - Travail collaboratif 

9h30-10h : Retour sur l’exercice

10h-11h29 : Les Chaînes modulaires et les espaces d'écriture collaborative

12h30-15h : Le gestionnaire de version Git & Initiation à GitLab

15h-15h29 : Présentation du travail final et de l'exercice à faire pour la séance 3

#### Séance 3 - Publication Web 

9h30-10h : Discussion sur le travail final

10h-11h29 : Écriture Web & Processus de publication

12h30-15h29 : Travail encadré sur les carnets d'écriture

## Modalités d’évaluation

Exercices durant les séances : 20%

Durant les séances de l'atelier, plusieurs exercices seront demandés à l’étudiant·e pour valider au fur et à mesure les acquis techniques. Cette évaluation se fera sur un mode continu.

Exercices hebdomadaires : 30%

Chaque semaine, en préparation de la séance prochaine, un exercice sera soumis à l’étudiant·e et sera discuté par la suite en classe. 

Travail final : 50%

Le travail final consiste à la création d'un carnet d'écriture en suivant la chaîne éditoriale qui aura été décrite durant l'atelier. L’objet d’édition du carnet d'écriture sera libre

## Intégrité

L’Université de Montréal s’est dotée de deux règlements disciplinaires sur la fraude et le plagiat, un qui s’adresse aux étudiant·e·s de premier cycle et l’autre à ceux et celles des cycles supérieurs :
<https://integrite.umontreal.ca/boite-a-outils/les-reglements>.

## Contact 

courriel : margot.mellet@umontreal.ca