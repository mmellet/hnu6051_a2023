--- 
title: Documentation
---

# Ressources

Cette page référence les documentations utiles au cours dont des vidéos explicatives sur l'usage des outils. 

-------------------------


## Raccourcis clavier 

### Gestion Écriture
- CTRL/Cmd + C : Copier (*Copy*)
- CTRL/Cmd + V : Coller (*Varnish*)
- CTRL/Cmd + X : Couper (*eXterminate*)

- CTRL/Cmd + A : Sélectionner tout (*All*)
- CTRL/Cmd + F : Rechercher (*Find*)

- CTRL/Cmd + Z : Annuler (*Zut*)
- CTRL/Cmd + Y : Revenir à l'état précédent (*Yesterday*)

### Gestion Document
- CTRL/Cmd + S : Sauvegarder (*Save*)
- CTRL/Cmd + O : Ouvrir (*Open*)
- CTRL/Cmd + Q : Quitter (*Quit*)

- CTRL/Cmd + F : Mettre en plein écran (*Full screen*)
- CTRL/Cmd + N : Nouvelle fenêtre (*New*)
### Traitement texte 
- CTRL/Cmd + B : Mise en gras (*Bold*)
- CTRL/Cmd + I : Mise en italique (*Italique*)
- CTRL/Cmd + U : Souligner (*Underline*)

-------------------------
-------------------------


## Markdown

### Niveaux de titre 

Les niveaux de titre se notent avec l'utilisation du dièse : 

```

## Titre de niveau 2 

### Titre de niveau 3 

```

### Italique et gras

```
- *italique*
- **gras** 
- ***gras italique***

```

### Notes 

```
Voici mon texte^[une note de bas de page inline.]

Voici mon texte[^1] 

[^1]:Une note de bas de page avec appel et renvoi

```

### Liens

```
[un lien vers une page Wikipédia](https://fr.wikipedia.org/wiki/Hyperlien)

```
 
### Image

```
![Légende](https://tonpetitlook.com/wp-content/uploads/2017/05/nouveau-chaton-nouveau-defi-430442.png)
```

### Tutoriel

<a href="https://www.arthurperret.fr/tutomd/" target="blank">Traduction en ligne par Arthur Perret</a>
	

-------------------------
-------------------------


## Stylo 

- [Documentation officielle](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)

-------------------------

-------------------------

<!--
## Zotero 

<iframe src="https://mmellet.github.io/FRA3825_2023/slides/Zotero.html" title="description" height="500" width="800" ></iframe>

<div style="text-align:center">
<a href="https://mmellet.github.io/FRA3825_2023/slides/Zotero.html" target="_blank">ouvrir dans mon navigateur</a>
</div>
-->