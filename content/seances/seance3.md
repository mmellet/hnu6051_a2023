--- 
title: "Séance 3"
date: 2023-09-18
---


# Séance du 19 septembre

## Présentation séance

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-1.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-1.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Chaînes de publication

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-2.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-2.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Travail final

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-3.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-3-3.html" target="_blank">ouvrir dans mon navigateur</a>
</div>
