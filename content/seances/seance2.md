--- 
title: "Séance 2"
date: 2023-09-12
---

# Séance du 12 septembre 

## Retour sur l'exercice & Bonnes pratiques d'édition

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-1.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-1.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Chaînes modulaires 

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-2.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-2.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Initiation à Git 

[Lien du groupe GitHub pour l'atelier](https://github.com/HNU6051-2023)

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-3.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-3.html" target="_blank">ouvrir dans mon navigateur</a>
</div>


## Présentation du travail final 

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-4.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-2-4.html" target="_blank">ouvrir dans mon navigateur</a>
</div>