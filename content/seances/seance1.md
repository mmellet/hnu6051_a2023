--- 
title: "Séance 1"
date: 2023-09-05
---

# Séance du 5 septembre
## Présentation du cours

<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-1.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-1.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Les Formats


<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-2.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-2.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Balisage en Markdown 


<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-3.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-3.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Éditeur de texte 


<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-4.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-4.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Références bibliographiques


<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-5.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-5.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

## Exercice pour séance 2 


<iframe src="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-6.html" title="description"  height="500" width="800" allowfullscreen="allowfullscreen"></iframe>

<div style="text-align:center">
<a href="https://mmellet.gitpages.huma-num.fr/hnu6051_a2023/slides/Seance-1-6.html" target="_blank">ouvrir dans mon navigateur</a>
</div>

