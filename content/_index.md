---
title : "HNU6051"
---

Bienvenue sur le site du cours **HNU6051 - Humanités numériques : formats d'écriture**. Il est destiné aux étudiant·e·s qui suivent l’atelier à l’automne 2023.

Le cours se déroule officiellement le mardi de 9h30 à 11h29, 12h30 à 15h29 les 5, 12 et 19 septembre.

-----

Le programme est disponible [ici](apropos).

Cet espace rassemble les différents éléments du cours : 

- le [contenu des séances](seances)
- les [ressources documentaires](documentation)
- mes [contacts](contact)
